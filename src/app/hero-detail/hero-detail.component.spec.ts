import { TestBed, ComponentFixture, fakeAsync, tick, flush, async } from '@angular/core/testing';
import { HeroDetailComponent } from './hero-detail.component';
import { ActivatedRoute } from '@angular/router';
import { HeroService } from '../hero.service';
import { Location } from '@angular/common';
// tslint:disable-next-line:import-blacklist
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';

describe('HeroDetailComponent', () => {
    let fixture: ComponentFixture<HeroDetailComponent>;
    let mockActivatedRoute, mockHeroService, mockLocation;

    beforeEach(() => {
        /**
         *  created the 'mockActivatedRoute' to deal with the ActivateRoute of '@angluar/router'
         */
        mockActivatedRoute = {
            snapshot: { paramMap: { get: () => '3' } }
        };
        mockHeroService = jasmine.createSpyObj(['getHero', 'updateHero']);
        mockLocation = jasmine.createSpyObj(['back']);

        TestBed.configureTestingModule({
            /**
             * imported the 'FormsModle' to deal with the ngModel in the form
             */
            imports: [FormsModule],
            declarations: [HeroDetailComponent],
            providers: [
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: HeroService, useValue: mockHeroService },
                { provide: Location, useValue: mockLocation }
            ]
        });
        fixture = TestBed.createComponent(HeroDetailComponent);
        mockHeroService.getHero.and.returnValue(of({ id: 3, name: 'SupperDude', strength: 100 }));
    });

    it('should render hero name in a h2 tag', () => {
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('h2').textContent).toContain('SUPPERDUDE');
    });

    // it('should call updateHero when save is called', async(() => {
    //     mockHeroService.updateHero.and.returnValue(of({}));
    //     fixture.detectChanges();

    //     fixture.componentInstance.save();
    //     /**
    //      * 'whenStable' is return the promise
    //      * this will helpfull for testing the async code
    //      * Note: this will only helpfull with the promise code.
    //      */
    //     fixture.whenStable().then(() => {
    //         expect(mockHeroService.updateHero).toHaveBeenCalled();
    //     });
    // }));

    it('should call updateHero when save is called',
        // (done) => {
        /**
         * fakeAsync is recommended with the flush
         */
        fakeAsync(() => {

            mockHeroService.updateHero.and.returnValue(of({}));
            fixture.detectChanges();

            fixture.componentInstance.save();
            /**
             * tick method of testing must require the 'time',
             * so this will only helpfull when you know the excecution time of you code.
             */
            // tick(250);

            /**
             * the flush() method of testing is very helpfull for checking the async code.
             * this method will check your code
             * that, if your code have the async function then it will wait to exectute.
             * or else it will checks the expectation.
             */
            flush();

            expect(mockHeroService.updateHero).toHaveBeenCalled();
        }));
    /**
     * the below code will only helpfull
     * when you have only one expectation for given time (aync code).
     * which can be using done function of asserstion
     */
    //     setTimeout(() => {
    //     expect(mockHeroService.updateHero).toHaveBeenCalled();
    //     done();
    // }, 300);
    // });
});
