import { TestBed, inject } from '@angular/core/testing';
import { HeroService } from './hero.service';
import { MessageService } from './message.service';

// for testing of httpClient injection in service.
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
describe('HeroService', () => {
    let mockMessageService;
    let httpTestingController: HttpTestingController;
    let service: HeroService;
    beforeEach(() => {
        mockMessageService = jasmine.createSpyObj(['add']);
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                HeroService,
                { provide: MessageService, useValue: mockMessageService }
            ]
        });

        /**
         * TestBed.get(HttpTestingController):- the get method is access the dependancy injection registry for accessing the given type
         * for ex. 'HttpTestingController'
         */
        httpTestingController = TestBed.get(HttpTestingController); // retrives the instance from the injector based on the provided token.
        service = TestBed.get(HeroService);


    }); // beforeEach
    describe('getHero', () => {
        it('should call get with the correct URL',
            /**
             * this is the another way to inject the services.
             */
            // inject([
            //     HeroService,
            //     HttpTestingController
            // ], (
            //     service: HeroService,
            //     contrller: HttpTestingController
            // ) => {

            //     })
            () => {

                /**
                 * the subscription will test and check that by using given URL the request is made or note.
                 * Note:- the subscription will called after the expectOne is called.
                 */
                service.getHero(4).subscribe();
                // service.getHero(3).subscribe();

                /**
                 * generates the http request for expecting it to be called.
                 */
                const req = httpTestingController.expectOne('api/heroes/4');

                /**
                 * to produce the demo output for http request.
                 */
                req.flush({ id: 4, name: 'SuperDude', strength: 100 });

                /**
                 * to match that this will call the exacet the expected URL.
                 */
                httpTestingController.verify();
            });
    }); // describe - 'getHero'
}); // describe - 'HeroService'
