import { ComponentFixture, TestBed } from "@angular/core/testing";
import { HeroComponent } from "./hero.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { By } from "@angular/platform-browser";

describe('HeroComponent (shallow tests)', () => {
    /**
     * will act as a Component with the template.
     */
    let fixture: ComponentFixture<HeroComponent>; // to get the instance of component like object and use it as a Component with it`s teplate.

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [HeroComponent],
            schemas: [NO_ERRORS_SCHEMA] // to prevent the Error for tempate Errors.
        });
        fixture = TestBed.createComponent(HeroComponent);// will create the component and returns the object.
    });

    it('should have the correct hero', () => {
        fixture.componentInstance.hero = { id: 1, name: 'SupperDude', strength: 3 };
        expect(fixture.componentInstance.hero.name).toEqual('SupperDude');
    });
    it('should render the hero name in an anchor tag', () => {
        fixture.componentInstance.hero = { id: 1, name: 'SupperDude', strength: 3 };
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('a').textContent).toContain('SupperDude');

        /**
         * to test by debugElement
         */
        let deA = fixture.debugElement.query(By.css('a'));
        expect(deA.nativeElement.textContent).toContain('SupperDude');
    });
});