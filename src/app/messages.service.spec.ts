import { MessageService } from "./message.service";

describe('MessageService', () => {
    let service: MessageService;

    /**
     * beforeEach method is used to executes all the pre-required 
     * expectation of it() methods 'test cases.'
     * which might be common to all
     */
    beforeEach(() => {

    });

    /**
     * to test the empty array before messages are inserted to Messages array.
     */
    it('should have no message to start', () => {
        service = new MessageService();

        expect(service.messages.length).toBe(0);
    });

    /**
     * to test the add() method.
     */
    it('should add a message when add is called', () => {
        service = new MessageService();

        service.add('message1');
        expect(service.messages.length).toBe(1);
    });
    
    /**
     * to test the clear() method.
     */
    it('should remove all messages when clear is called', () => {
        service = new MessageService();
        service.add('message1');

        service.clear()
        expect(service.messages.length).toBe(0);
    });
});