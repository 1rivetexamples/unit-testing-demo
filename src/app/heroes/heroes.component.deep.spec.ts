import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HeroesComponent } from './heroes.component';
import { NO_ERRORS_SCHEMA, Component, Input, Directive } from '@angular/core';
import { HeroService } from '../hero.service';
import { of } from 'rxjs';
import { Hero } from '../hero';
import { By } from '@angular/platform-browser';
import { HeroComponent } from '../hero/hero.component';

@Directive({
    selector: '[routerLink]',
    host: { '(click)': 'onClick()' }
})
export class RouterLinkDirectiveStub {
    @Input('routerLink') linkParams: any;
    navigatedTo: any = null;

    onClick = () => {
        this.navigatedTo = this.linkParams;
    }
}

describe('HeroesComponent (Deep)', () => {
    let fixture: ComponentFixture<HeroesComponent>;
    let mockHeroService;
    let HEROES;

    beforeEach(() => {
        /**
         * dummy Data.
         */
        HEROES = [
            { id: 1, name: 'SpiderDude', strength: 8 },
            { id: 2, name: 'Wonderful Women', strength: 24 },
            { id: 3, name: 'SupperDude', strength: 55 }
        ];

        /**
         * creating mock service object.
         */
        mockHeroService = jasmine.createSpyObj(['getHeroes', 'addHero', 'deleteHero']);

        /**
         * creating the module references for component testing.
         */
        TestBed.configureTestingModule({
            declarations: [
                HeroesComponent,
                HeroComponent,
                RouterLinkDirectiveStub
            ],
            providers: [
                { provide: HeroService, useValue: mockHeroService }
            ],
            // schemas: [NO_ERRORS_SCHEMA]
        });

        /**
         * will creates the component
         */
        fixture = TestBed.createComponent(HeroesComponent);

    });
    it('should render each hero as a HeroComponent', () => {
        mockHeroService.getHeroes.and.returnValue(of(HEROES));
        /**
         * run ngOnInit
         */
        fixture.detectChanges();

        /**
         * By.directive will select the compoent as a directive
         *
         * means in angular the component is a directive
         */
        const HeroComponentDEs = fixture.debugElement.queryAll(By.directive(HeroComponent));
        expect(HeroComponentDEs.length).toEqual(3);

        /**
         * Check for all the single hero of HeroCoponent is
         * same as passed by the parent component HeroesComponent.
         *
         * Note:- this testing for child component using deep integration testing technique.
         */
        for (let i = 0; i < HeroComponentDEs.length; i++) {
            const heroComponentDEs = HeroComponentDEs[i];
            expect(HeroComponentDEs[i].componentInstance.hero).toEqual(HEROES[i]);
        }

    });

    it('should call heroService.deleteHero when the Hero Component`s delete button is clicked', () => {
        spyOn(fixture.componentInstance, 'delete');
        mockHeroService.getHeroes.and.returnValue(of(HEROES));

        fixture.detectChanges();

        const heroComponents = fixture.debugElement.queryAll(By.directive(HeroComponent));
        // heroComponents[0].query(By.css('button'))
        //     .triggerEventHandler('click', { stopPropagation: () => { } });

        /**
         * Emmitting Event from Children.
         * here, we are asking that, please call the delete method that you have.
         */
        // (<HeroComponent>heroComponents[0].componentInstance).delete.emit(undefined);

        /**
         * Raising Events on Child Directives.
         * here, we are directly calling calling the method of chiled component.
         */
        heroComponents[0].triggerEventHandler('delete', null);

        expect(fixture.componentInstance.delete).toHaveBeenCalledWith(HEROES[0]);
    });

    it('should add a new hero to the list when the add button is clicked', () => {
        mockHeroService.getHeroes.and.returnValue(of(HEROES));
        fixture.detectChanges();
        const name = 'Mr. AFZAL';

        mockHeroService.addHero.and.returnValue(of({ id: 5, name: name, strength: 4 }));
        const inputElement = fixture.debugElement.query(By.css('input')).nativeElement;
        const AddButton = fixture.debugElement.queryAll(By.css('button'))[0];

        inputElement.value = name;
        AddButton.triggerEventHandler('click', null);
        fixture.detectChanges();

        const heroText = fixture.debugElement.query(By.css('ul')).nativeElement.textContent;

        expect(heroText).toContain(name);

    });

    /**
     * to test the routerLink
     */
    it('should have the correct route for the first hero', () => {
        /**
         * for ngOnInit
         */
        mockHeroService.getHeroes.and.returnValue(of(HEROES));
        fixture.detectChanges();

        const heroComponents = fixture.debugElement.queryAll(By.directive(HeroComponent));
        /**
         * Note:- 1) injector.get will get the injected directive (i.e. 'RouterLinkDirectiveStub' for 'routerLink')
         */
        const routerLink = heroComponents[0]
            .query(By.directive(RouterLinkDirectiveStub))
            .injector.get(RouterLinkDirectiveStub);

        heroComponents[0].query(By.css('a')).triggerEventHandler('click', null);

        expect(routerLink.navigatedTo).toBe('/detail/1');
    });
});
