import { ComponentFixture, TestBed } from "@angular/core/testing";
import { HeroesComponent } from "./heroes.component";
import { NO_ERRORS_SCHEMA, Component, Input } from "@angular/core";
import { HeroService } from "../hero.service";
import { of } from "rxjs";
import { Hero } from "../hero";
import { By } from "@angular/platform-browser";

describe('HeroesComponent (Shallow)', () => {
    let fixture: ComponentFixture<HeroesComponent>;
    let mockHeroService;
    let HEROES;

    /**
     * creating fake chiled component.
     */
    @Component({
        selector: 'app-hero',
        template: '<div><div>'
    })
    class FakeHeroComponent {
        @Input() hero: Hero;
    }

    beforeEach(() => {
        /**
         * dummy Data.
         */
        HEROES = [
            { id: 1, name: 'SpiderDude', strength: 8 },
            { id: 2, name: 'Wonderful Women', strength: 24 },
            { id: 3, name: 'SupperDude', strength: 55 }
        ];

        /**
         * creating mock service object.
         */
        mockHeroService = jasmine.createSpyObj(['getHeroes', 'addHero', 'deleteHero']);

        /**
         * creating the module references for component testing.
         */
        TestBed.configureTestingModule({
            declarations: [
                HeroesComponent,
                FakeHeroComponent
            ],
            providers: [
                { provide: HeroService, useValue: mockHeroService }
            ],
            // schemas: [NO_ERRORS_SCHEMA]
        });

        /**
         * will creates the component
         */
        fixture = TestBed.createComponent(HeroesComponent);
    });

    it('should set heroes currently from the service', () => {
        mockHeroService.getHeroes.and.returnValue(of(HEROES));// working as an injected service and call the 'getHeroes()' method.
        fixture.detectChanges(); // will calles the lifecycle method 
        expect(fixture.componentInstance.heroes.length).toBe(3);
    });

    it('should create one li for each heros', () => {
        mockHeroService.getHeroes.and.returnValue(of(HEROES));
        fixture.detectChanges();
        expect(fixture.debugElement.queryAll(By.css('li')).length).toBe(3);
    })
});