import { HeroesComponent } from "./heroes.component";
import { of } from "rxjs";

describe('HeroesComponent', () => {
    let component: HeroesComponent;
    let HEROES;
    let mockHeroService;

    beforeEach(() => {
        HEROES = [
            { id: 1, name: 'SpiderDude', strength: 8 },
            { id: 2, name: 'Wonderful Women', strength: 24 },
            { id: 3, name: 'SuperDude', strength: 55 }
        ];

        /**
         * creating the mock Object. which is called 'mocking',
         * becouse this HeroService is injecting the 
         * various dependecies in the constructor.
         */
        mockHeroService = jasmine.createSpyObj(['getHeroes', 'addHero', 'deleteHero']);

        component = new HeroesComponent(mockHeroService);
    });

    describe('delete', () => {
        it('should remove th indicated hero from the heroes list', () => {
            mockHeroService.deleteHero.and.returnValue(of(true)); // this is preventing the undefined of subscribe method calling which is the mehtod of observable.
            component.heroes = HEROES;

            component.delete(HEROES[2]);

            expect(component.heroes.length).toBe(2);
        });
        /**
         * this testing is based on mock object of service.
         * which will check for the method of service as expected.
         */
        it('should call deleteHero', () => {
            mockHeroService.deleteHero.and.returnValue(of(true));
            component.heroes = HEROES;

            component.delete(HEROES[2]);

            expect(mockHeroService.deleteHero).toHaveBeenCalledWith(HEROES[2]); // to check that deleteHero() method is called as expected ?
        });
    });
});